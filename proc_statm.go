//go:build linux

/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package proc

import (
	"bytes"
	"fmt"
	"os"
)

func (p Proc) StatM() (StatM, error) {
	data, err := p.Read("statM")
	if err != nil {
		return StatM{}, err
	}

	var statM StatM

	if _, err = fmt.Fscan(bytes.NewReader(data),
		&statM.Size,
		&statM.Resident,
		&statM.Shared,
		&statM.TRS,
		&statM.LRS,
		&statM.DRS,
		&statM.DT,
	); err != nil {
		return StatM{}, err
	}

	page := uint64(os.Getpagesize())

	statM.Size *= page
	statM.Resident *= page
	statM.Shared *= page
	statM.TRS *= page
	statM.LRS *= page
	statM.DRS *= page
	statM.DT *= page

	return statM, nil
}

// StatM Provides information about memory usage, measured in pages.
type StatM struct {
	// Total program size (page) (same as VmSize in status)
	Size uint64

	// Size of memory portions (page) (same as VmRSS in status)
	Resident uint64

	// Number of pages that are shared (i.e. backed by a file, same as RssFile+RssShmem in status)
	Shared uint64

	// Number of pages that are 'code' (not including libs; broken, includes data segment)
	TRS uint64

	// Number of pages of library (always 0 on 2.6)
	LRS uint64

	// Number of pages of data/stack (including libs; broken, includes library text)
	DRS uint64

	// Number of dirty pages (always 0 on 2.6)
	DT uint64
}
