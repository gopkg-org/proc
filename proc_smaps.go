//go:build linux

/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package proc

import (
	"bufio"
	"bytes"
	"reflect"
	"strconv"
	"strings"

	"gopkg.org/size"
)

func (p Proc) SMaps() (SMaps, error) {
	data, err := p.Read("smaps")
	if err != nil {
		return nil, err
	}

	sMaps, sMap := make(SMaps, 0), SMap{}

	scanner := bufio.NewScanner(bytes.NewReader(data))

	for scanner.Scan() {
		if len(strings.TrimSpace(scanner.Text())) == 0 {
			continue
		}

		if strings.Contains(scanner.Text(), "-") {
			if !reflect.DeepEqual(sMap, SMap{}) {
				sMaps = append(sMaps, sMap)
			}

			sMap.Map, err = parseMapHeader(scanner.Text())
			if err != nil {
				return nil, err
			}

			continue
		}

		kv := strings.SplitN(scanner.Text(), ":", 2)

		k, v := strings.TrimSpace(kv[0]), strings.TrimSpace(kv[1])

		switch k {
		case "Size":
			sMap.Size, _ = size.ParseToBytes(v)
		case "KernelPageSize":
			sMap.KernelPageSize, _ = size.ParseToBytes(v)
		case "MMUPageSize":
			sMap.MMUPageSize, _ = size.ParseToBytes(v)
		case "Rss":
			sMap.RSS, _ = size.ParseToBytes(v)
		case "Pss":
			sMap.PSS, _ = size.ParseToBytes(v)
		case "Shared_Clean":
			sMap.SharedClean, _ = size.ParseToBytes(v)
		case "Shared_Dirty":
			sMap.SharedDirty, _ = size.ParseToBytes(v)
		case "Private_Clean":
			sMap.PrivateClean, _ = size.ParseToBytes(v)
		case "Private_Dirty":
			sMap.PrivateDirty, _ = size.ParseToBytes(v)
		case "Referenced":
			sMap.Referenced, _ = size.ParseToBytes(v)
		case "Anonymous":
			sMap.Anonymous, _ = size.ParseToBytes(v)
		case "LazyFree":
			sMap.LazyFree, _ = size.ParseToBytes(v)
		case "AnonHugePages":
			sMap.AnonHugePages, _ = size.ParseToBytes(v)
		case "ShmemPmdMapped":
			sMap.ShMemPmdMapped, _ = size.ParseToBytes(v)
		case "FilePmdMapped":
			sMap.FilePmdMapped, _ = size.ParseToBytes(v)
		case "Shared_Hugetlb":
			sMap.SharedHugeTLB, _ = size.ParseToBytes(v)
		case "Private_Hugetlb":
			sMap.PrivateHugeTLB, _ = size.ParseToBytes(v)
		case "Swap":
			sMap.Swap, _ = size.ParseToBytes(v)
		case "SwapPss":
			sMap.SwapPss, _ = size.ParseToBytes(v)
		case "Locked":
			sMap.Locked, _ = size.ParseToBytes(v)
		case "THPeligible":
			sMap.THPEligible, _ = strconv.ParseBool(v)
		case "VmFlags":
			sMap.VmFlags = strings.Fields(v)
		}
	}

	if err = scanner.Err(); err != nil {
		return nil, err
	}

	return sMaps, nil
}

type SMap struct {
	// Same information as is displayed for the mapping in /proc/pid/maps
	Map

	// Size of the mapping
	Size uint64

	// KernelPageSize line (available since Linux 2.6.29) is the page size used by the kernel to back the virtual
	// memory area. This matches the size used by the MMU in the majority of cases. However, one counter-example
	// occurs on PPC64 kernels whereby a kernel using 64 kB as a base page size may still use 4 kB pages for the
	// MMU on older processors.
	KernelPageSize uint64

	// MMUPageSize line (also available since Linux 2.6.29) reports the page size used by the MMU.
	MMUPageSize uint64

	// RSS is the amount of the mapping that is currently resident in RAM
	RSS uint64

	// PSS is the process's proportional share of this mapping
	PSS uint64

	// SharedClean, SharedDirty, PrivateClean and PrivateDirty are the numbers of clean and dirty shared pages in
	// the mapping, and the number of clean and dirty private pages in the mapping
	SharedClean, SharedDirty, PrivateClean, PrivateDirty uint64

	// Referenced indicates the amount of memory currently marked as referenced or accessed
	Referenced uint64

	// Anonymous shows the amount of memory that does not belong to any file.
	Anonymous uint64

	// LazyFree shows the amount of memory which is marked by madvise(MADV_FREE). The memory isn't freed immediately
	// with madvise(). It's freed in memory pressure if the memory is clean. Please note that the printed value might
	// be lower than the real value due to optimizations used in the current implementation. If this is not desirable
	// please file a bug report.
	LazyFree uint64

	// AnonHugePages shows the amount of memory backed by transparent hugepage.
	AnonHugePages uint64

	// ShMemPmdMapped shows the amount of shared (shmem/tmpfs) memory backed by huge pages.
	ShMemPmdMapped uint64

	//
	FilePmdMapped uint64

	// SharedHugeTLB and PrivateHugeTLB" show the amounts of memory backed by hugetlbfs page which is not counted in
	// RSS or PSS field for historical reasons. And these are not included in {Shared,Private}{Clean,Dirty} field.
	SharedHugeTLB, PrivateHugeTLB uint64

	// Swap shows how much would-be-anonymous memory is also used, but out on swap.
	Swap uint64

	// SwapPss shows proportional swap share of this mapping.
	// Unlike Swap, this does not take into account swapped out page of underlying shmem objects.
	SwapPss uint64

	// Locked indicates whether the mapping is locked in memory or not.
	Locked uint64

	// THPEligible indicates whether the mapping is eligible for allocating THP pages as well as the THP is PMD
	// mappable or not - 1 if true, 0 otherwise. It just shows the current status
	THPEligible bool

	// VmFlags line (available since Linux 3.8) represents the kernel flags associated with the virtual memory area,
	// encoded using the following two-letter codes:
	//     rd   -   readable
	//     wr   -   writable
	//     ex   -   executable
	//     sh   -   shared
	//     mr   -   may read
	//     mw   -   may write
	//     me   -   may execute
	//     ms   -   may share
	//     gd   -   stack segment grows down
	//     pf   -   pure PFN range
	//     dw   -   disabled write to the mapped file
	//     lo   -   pages are locked in memory
	//     io   -   memory mapped I/O area
	//     sr   -   sequential read advise provided
	//     rr   -   random read advise provided
	//     dc   -   do not copy area on fork
	//     de   -   do not expand area on remapping
	//     ac   -   area is accountable
	//     nr   -   swap space is not reserved for the area
	//     ht   -   area uses huge tlb pages
	//     sf   -   perform synchronous page faults (since Linux 4.15)
	//     nl   -   non-linear mapping (removed in Linux 4.0)
	//     ar   -   architecture specific flag
	//     wf   -   wipe on fork (since Linux 4.14)
	//     dd   -   do not include area into core dump
	//     sd   -   soft-dirty flag (since Linux 3.13)
	//     mm   -   mixed map area
	//     hg   -   huge page advise flag
	//     nh   -   no-huge page advise flag
	//     mg   -   mergeable advise flag
	//     um   -   userfaultfd missing pages tracking (since Linux 4.3)
	//     uw   -   userfaultfd wprotect pages tracking (since Linux 4.3)
	VmFlags []string
}

type SMaps []SMap

func (s SMaps) Size() uint64 {
	var sz uint64

	for _, m := range s {
		sz += m.Size
	}

	return sz
}

func (s SMaps) RSS() uint64 {
	var rss uint64

	for _, m := range s {
		rss += m.RSS
	}

	return rss
}

func (s SMaps) PSS() uint64 {
	var pss uint64

	for _, m := range s {
		pss += m.PSS
	}

	return pss
}

func (s SMaps) USS() uint64 {
	var uss uint64

	for _, m := range s {
		uss += m.PrivateClean + m.PrivateDirty
	}

	return uss
}

func (s SMaps) Swap() uint64 {
	var swap uint64

	for _, m := range s {
		swap += m.Swap
	}

	return swap
}
