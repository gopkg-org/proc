//go:build linux

/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package proc

import (
	"bytes"
	"fmt"
	"strings"
)

func (p Proc) Stat() (Stat, error) {
	data, err := p.Read("stat")
	if err != nil {
		return Stat{}, err
	}

	var stat Stat

	if _, err = fmt.Fscan(bytes.NewReader(data),
		&stat.PID,
		&stat.Comm,
		&stat.State,
		&stat.PPID,
		&stat.PGRP,
		&stat.Session,
		&stat.TTYNR,
		&stat.TPGID,
		&stat.Flags,
		&stat.MINFLT,
		&stat.CMINFLT,
		&stat.MAJFLT,
		&stat.CMAJFLT,
		&stat.UTime,
		&stat.STime,
		&stat.CUTime,
		&stat.CSTime,
		&stat.Priority,
		&stat.Nice,
		&stat.NumThreads,
		&stat.ItRealValue,
		&stat.StartTime,
		&stat.VSIZE,
		&stat.RSS,
		&stat.RSSLim,
		&stat.StartCode,
		&stat.EndCode,
		&stat.StartStack,
		&stat.KSTKESP,
		&stat.KSTKEIP,
		&stat.Signal,
		&stat.Blocked,
		&stat.SigIgnore,
		&stat.SigCatch,
		&stat.WChan,
		&stat.NSwap,
		&stat.CNSwap,
		&stat.ExitSignal,
		&stat.Processor,
		&stat.RTPriority,
		&stat.Policy,
		&stat.DelayACCTBLKIOTicks,
		&stat.GuestTime,
		&stat.CGuestTime,
		&stat.StartData,
		&stat.EndData,
		&stat.StartBRK,
		&stat.ArgStart,
		&stat.ArgEnd,
		&stat.EnvStart,
		&stat.EnvEnd,
		&stat.ExitCode,
	); err != nil {
		return Stat{}, err
	}

	// Remove parentheses of Comm.
	stat.Comm = strings.TrimSuffix(strings.TrimPrefix(stat.Comm, "("), ")")

	return stat, nil
}

// Stat information about the process.  This is used by ps(1). It is defined in the kernel source file fs/proc/array.c.
//
//	The fields, in order, with their proper scanf(3) format specifiers, are listed below. Whether or not certain of
//	these fields display valid information is governed by a ptrace access mode PTRACE_MODE_READ_FSCREDS |
//	PTRACE_MODE_NOAUDIT check (refer to ptrace(2)). If the check denies access, then the field value is displayed as 0.
//	The affected fields are indicated with the marking [PT].
type Stat struct {
	// The process ID.
	PID int32

	// The filename of the executable, in parentheses.
	// Strings longer than TASK_COMM_LEN (16) characters (including the terminating null byte) are silently truncated.
	// This is visible whether or not the executable is swapped out.
	Comm string

	// One of the following characters, indicating process state:
	// 		R      Running
	// 		S      Sleeping in an interruptible wait
	// 		D      Waiting in uninterruptible disk sleep
	// 		Z      Zombie
	// 		T      Stopped (on a signal) or (before Linux 2.6.33) trace stopped
	// 		t      Tracing stop (Linux 2.6.33 onward)
	// 		W      Paging (only before Linux 2.6.0)
	// 		X      Dead (from Linux 2.6.0 onward)
	// 		x      Dead (Linux 2.6.33 to 3.13 only)
	// 		K      Wakekill (Linux 2.6.33 to 3.13 only)
	// 		W      Waking (Linux 2.6.33 to 3.13 only)
	// 		P      Parked (Linux 3.9 to 3.13 only)
	// 		I      Idle (Linux 4.14 onward)
	State rune

	// The PID of the parent of this process.
	PPID int32

	// The process group ID of the process.
	PGRP int32

	// The session ID of the process.
	Session int32

	// The controlling terminal of the process.
	// (The minor device number is contained in the combination of bits 31 to 20 and 7 to 0; the major device number
	// is in bits 15 to 8.)
	TTYNR int32

	// The ID of the foreground process group of the controlling terminal of the process.
	TPGID int32

	// The kernel flags word of the process.
	// For bit meanings, see the PF_* defines in the Linux kernel source file include/linux/sched.h.
	// Details depend on the kernel version.
	// The format for this field was %lu before Linux 2.6.
	Flags uint32

	// The number of minor faults the process has made which have not required loading a memory page from disk.
	MINFLT uint64

	// The number of minor faults that the process's waited-for children have made.
	CMINFLT uint64

	// The number of major faults the process has made which have required loading a memory page from disk.
	MAJFLT uint64

	// The number of major faults that the process's waited-for children have made.
	CMAJFLT uint64

	// Amount of time that this process has been scheduled in user mode, measured in clock ticks (divide by
	// sysconf(_SC_CLK_TCK)).
	// This includes guest time, guest_time (time spent running a virtual CPU, see below), so that applications that
	// are not aware of the guest time field do not lose that time from their calculations.
	UTime uint64

	// Amount of time that this process has been scheduled in kernel mode, measured in clock ticks (divide by
	// sysconf(_SC_CLK_TCK)).
	STime uint64

	// Amount of time that this process's waited-for children have been scheduled in user mode, measured in clock
	// ticks (divide by sysconf(_SC_CLK_TCK)). (See also times(2).)
	//This includes guest time, cguest_time (time spent running a virtual CPU, see below).
	CUTime int64

	// Amount of time that this process's waited-for children have been scheduled in kernel mode, measured in clock
	// ticks (divide by sysconf(_SC_CLK_TCK)).
	CSTime int64

	// (Explanation for Linux 2.6)
	// For processes running a real-time scheduling policy (policy below see sched_setscheduler(2)), this is the
	// negated scheduling priority, minus one that is, a number in the range -2 to -100, corresponding to real-time
	// priorities 1 to 99.  For processes running under a non-real-time scheduling policy, this is the raw nice value
	// (setpriority(2)) as represented in the kernel.The kernel stores nice values as numbers in the range 0 (high) to
	// 39 (low), corresponding to the user-visible nice range of -20 to 19.
	// Before Linux 2.6, this was a scaled value based on the scheduler weighting given to this process.
	Priority int64

	// The nice value (see setpriority(2)), a value in the range 19 (low priority) to -20 (high priority).
	Nice int64

	// Number of threads in this process (since Linux 2.6).
	// Before Linux 2.6, this field was hard coded to 0 as a placeholder for an earlier removed field.
	NumThreads int64

	// The time in jiffies before the next SIGALRM is sent to the process due to an interval timer.
	// Since Linux 2.6.17, this field is no longer maintained, and is hard coded as 0.
	ItRealValue int64

	// The time the process started after system boot.
	// Before Linux 2.6, this value was expressed in jiffies.
	// Since Linux 2.6, the value is expressed in clock ticks (divide by sysconf(_SC_CLK_TCK)).
	// The format for this field was %lu before Linux 2.6.
	StartTime uint64

	// Virtual memory size in bytes.
	VSIZE uint64

	// Resident Set Size: number of pages the process has in real memory.
	// This is just the pages which count toward text, data, or stack space.
	// This does not include pages which have not been demand-loaded in, or which are swapped out.
	// This value is inaccurate see /proc/pid/statm below.
	RSS int64

	// Current soft limit in bytes on the rss of the process see the description of RLIMIT_RSS in getrlimit(2).
	RSSLim uint64

	// The address above which program text can run.
	StartCode uint64

	// The address below which program text can run.
	EndCode uint64

	// The address of the start (i.e., bottom) of the stack.
	StartStack uint64

	// The current value of ESP (stack pointer), as found in the kernel stack page for the process.
	KSTKESP uint64

	// The current EIP (instruction pointer).
	KSTKEIP uint64

	// The bitmap of pending signals, displayed as a decimal number.
	// Obsolete, because it does not provide information on real-time signals use /proc/pid/status instead.
	Signal uint64

	// The bitmap of blocked signals, displayed as a decimal number.
	// Obsolete, because it does not provide information on real-time signals use /proc/pid/status instead.
	Blocked uint64

	// The bitmap of ignored signals, displayed as a decimal number.
	// Obsolete, because it does not provide information on real-time signals use /proc/pid/status instead.
	SigIgnore uint64

	// The bitmap of caught signals, displayed as a decimal number.
	// Obsolete, because it does not provide information on real-time signals use /proc/pid/status instead.
	SigCatch uint64

	// This is the "channel" in which the process is waiting.
	// It is the address of a location in the kernel where the process is sleeping.
	// The corresponding symbolic name can be found in /proc/pid/wchan.
	WChan uint64

	// Number of pages swapped (not maintained).
	NSwap uint64

	// Cumulative nswap for child processes (not maintained).
	CNSwap uint64

	// Signal to be sent to parent when we die.
	// (since Linux 2.1.22)
	ExitSignal int32

	// CPU number last executed on.
	// (since Linux 2.2.8)
	Processor int32

	// Real-time scheduling priority, a number in the range 1 to 99 for processes scheduled under a real-time policy,
	// or 0, for non-real-time processes (see sched_setscheduler(2)).
	// (since Linux 2.5.19)
	RTPriority uint32

	// Scheduling policy (see sched_setscheduler(2)).
	// Decode using the SCHED_* constants in linux/sched.h.
	// The format for this field was %lu before Linux 2.6.22.
	// (since Linux 2.5.19)
	Policy uint32

	// Aggregated block I/O delays, measured in clock ticks (centiseconds).
	// (since Linux 2.6.18)
	DelayACCTBLKIOTicks uint64

	// Guest time of the process (time spent running a virtual CPU for a guest operating system), measured in
	// clock ticks (divide by sysconf(_SC_CLK_TCK)).
	// (since Linux 2.6.24)
	GuestTime uint64

	// Guest time of the process's children, measured in clock ticks (divide by sysconf(_SC_CLK_TCK)).
	// (since Linux 2.6.24)
	CGuestTime int64

	// Address above which program initialized and uninitialized (BSS) data are placed.
	// (since Linux 3.3)
	StartData uint64

	// Address below which program initialized and uninitialized (BSS) data are placed.
	// (since Linux 3.3)
	EndData uint64

	// Address above which program heap can be expanded with brk(2).
	// (since Linux 3.3)
	StartBRK uint64

	// Address above which program command-line arguments (argv) are placed.
	// (since Linux 3.5)
	ArgStart uint64

	// Address below program command-line arguments (argv) are placed.
	// (since Linux 3.5)
	ArgEnd uint64

	// Address above which program environment is placed.
	// (since Linux 3.5)
	EnvStart uint64

	// Address below which program environment is placed.
	// (since Linux 3.5)
	EnvEnd uint64

	// The thread's exit status in the form reported by waitpid(2).
	// (since Linux 3.5)
	ExitCode int32
}
