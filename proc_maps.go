//go:build linux

/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package proc

import (
	"bufio"
	"bytes"
	"errors"
	"strconv"
	"strings"

	"golang.org/x/sys/unix"
)

// Maps file containing the currently mapped memory regions and their access permissions.
func (p Proc) Maps() (Maps, error) {
	data, err := p.Read("maps")
	if err != nil {
		return nil, err
	}

	maps := make(Maps, 0)

	scanner := bufio.NewScanner(bytes.NewReader(data))

	for scanner.Scan() {
		if len(strings.TrimSpace(scanner.Text())) == 0 {
			continue
		}

		m, err := parseMapHeader(scanner.Text())
		if err != nil {
			return nil, err
		}

		maps = append(maps, m)
	}

	if err = scanner.Err(); err != nil {
		return nil, err
	}

	return maps, nil
}

type Map struct {
	// AddressStart and AddressEnd is the address space in the process that it occupies
	AddressStart, AddressEnd uintptr

	// Permissions is a set of permissions
	Permissions MapPermissions

	// Offset is the offset into the file/whatever
	Offset int64

	// Device is the device (major:minor)
	Device uint64

	// INode the inode on that device
	// 0 indicates that no inode is associated with the memory region, as the case would be with BSS (uninitialized data).
	INode uint64

	// PathName field will usually be the file that is backing the mapping
	PathName string
}

type Maps []Map

type MapPermissions struct {
	Read, Write, Execute, Shared, Private bool
}

func parseMapHeader(line string) (Map, error) {
	values := strings.Fields(line)
	if len(values) < 5 {
		return Map{}, errors.New("invalid map header")
	}

	var (
		header Map
		err    error
	)

	header.AddressStart, header.AddressEnd, err = parseMapAddresses(values[0])
	if err != nil {
		return Map{}, err
	}

	header.Permissions, err = parseMapPermissions(values[1])
	if err != nil {
		return Map{}, err
	}

	header.Offset, err = strconv.ParseInt(values[2], 16, 0)
	if err != nil {
		return Map{}, err
	}

	header.Device, err = parseMapDevices(values[3])
	if err != nil {
		return Map{}, err
	}

	header.INode, err = strconv.ParseUint(values[4], 10, 0)
	if err != nil {
		return Map{}, err
	}

	if len(values) > 5 {
		header.PathName = strings.TrimSpace(strings.Join(values[5:], " "))
	}

	return header, nil
}

func parseMapAddresses(s string) (uintptr, uintptr, error) {
	values := strings.Split(s, "-")
	if len(values) != 2 {
		return 0, 0, errors.New("invalid addresses")
	}

	addressStart, err := strconv.ParseUint(values[0], 16, 0)
	if err != nil {
		return 0, 0, err
	}

	addressEnd, err := strconv.ParseUint(values[1], 16, 0)
	if err != nil {
		return 0, 0, err
	}

	return uintptr(addressStart), uintptr(addressEnd), nil
}

func parseMapPermissions(s string) (MapPermissions, error) {
	if len(s) != 4 {
		return MapPermissions{}, errors.New("invalid permissions")
	}

	var perms MapPermissions
	for _, c := range s {
		switch c {
		case 'r':
			perms.Read = true
		case 'w':
			perms.Write = true
		case 'x':
			perms.Execute = true
		case 's':
			perms.Shared = true
		case 'p':
			perms.Private = true
		}
	}
	return perms, nil
}

func parseMapDevices(s string) (uint64, error) {
	values := strings.Split(s, ":")
	if len(values) != 2 {
		return 0, errors.New("invalid device")
	}

	deviceMajor, err := strconv.ParseUint(values[0], 16, 0)
	if err != nil {
		return 0, err
	}

	deviceMinor, err := strconv.ParseUint(values[1], 16, 0)
	if err != nil {
		return 0, err
	}

	return unix.Mkdev(uint32(deviceMajor), uint32(deviceMinor)), nil
}
