# Proc - Linux

Provides a library to read information from `/proc`, based on 
[https://man7.org/linux/man-pages/man5/proc.5.html](https://man7.org/linux/man-pages/man5/proc.5.html) documentation.

## Status

- [ ] `/proc/pid/attr`
- [ ] `/proc/pid/attr/current`
- [ ] `/proc/pid/attr/exec`
- [ ] `/proc/pid/attr/fscreate`
- [ ] `/proc/pid/attr/keycreate`
- [ ] `/proc/pid/attr/prev`
- [ ] `/proc/pid/attr/socketcreate`
- [ ] `/proc/pid/autogroup`
- [ ] `/proc/pid/auxv`
- [ ] `/proc/pid/cgroup`
- [ ] `/proc/pid/clear_refs`
- [x] `/proc/pid/cmdline`
- [ ] `/proc/pid/comm`
- [ ] `/proc/pid/coredump_filter`
- [ ] `/proc/pid/cpuset`
- [ ] `/proc/pid/cwd`
- [ ] `/proc/pid/environ`
- [ ] `/proc/pid/exe`
- [ ] `/proc/pid/fd/`
- [ ] `/proc/pid/fdinfo/`
- [ ] `/proc/pid/gid_map`
- [ ] `/proc/pid/io`
- [ ] `/proc/pid/limits`
- [ ] `/proc/pid/map_files/`
- [x] `/proc/pid/maps`
- [ ] `/proc/pid/mem`
- [ ] `/proc/pid/mountinfo`
- [ ] `/proc/pid/mounts`
- [ ] `/proc/pid/mountstats`
- [ ] `/proc/pid/net`
- [ ] `/proc/pid/ns/`
- [ ] `/proc/pid/numa_maps`
- [ ] `/proc/pid/oom_adj`
- [ ] `/proc/pid/oom_score`
- [ ] `/proc/pid/oom_score_adj`
- [ ] `/proc/pid/pagemap`
- [ ] `/proc/pid/personality`
- [ ] `/proc/pid/root`
- [ ] `/proc/pid/projid_map`
- [ ] `/proc/pid/seccomp`
- [ ] `/proc/pid/setgroups`
- [x] `/proc/pid/smaps`
- [ ] `/proc/pid/stack`
- [x] `/proc/pid/stat`
- [x] `/proc/pid/statm`
- [ ] `/proc/pid/status`
- [ ] `/proc/pid/syscall`
- [ ] `/proc/pid/task`
- [ ] `/proc/pid/task/tid/children`
- [ ] `/proc/pid/timers`
- [ ] `/proc/pid/timerslack_ns`
- [ ] `/proc/pid/uid_map`
- [ ] `/proc/pid/wchan`
