//go:build linux

/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package proc

import (
	"io"
	"os"
	"os/user"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"syscall"
)

const BasePath = "/proc"

func All() (Procs, error) {
	dirs, err := os.ReadDir(BasePath)
	if err != nil {
		return nil, err
	}

	procs := make(Procs, 0)

	for _, dir := range dirs {
		if !dir.IsDir() {
			continue
		}

		pid, err := strconv.ParseInt(dir.Name(), 10, 64)
		if err != nil {
			continue
		}

		proc, err := New(int(pid))
		if err != nil {
			return nil, err
		}

		procs = append(procs, proc)
	}

	sort.Sort(procs)

	return procs, nil
}

func Self() (Proc, error) {
	p, err := os.Readlink(filepath.Join(BasePath, "self"))
	if err != nil {
		return Proc{}, err
	}

	pid, err := strconv.Atoi(p)
	if err != nil {
		return Proc{}, err
	}

	return New(pid)
}

func New(pid int) (Proc, error) {
	stat, err := os.Stat(filepath.Join(BasePath, strconv.Itoa(pid)))
	if err != nil {
		return Proc{}, err
	}

	var (
		usr *user.User
		grp *user.Group
	)

	if sys, ok := stat.Sys().(*syscall.Stat_t); ok {
		usr, err = user.LookupId(strconv.Itoa(int(sys.Uid)))
		if err != nil {
			usr = &user.User{Uid: strconv.Itoa(int(sys.Uid))}
		}
		grp, err = user.LookupGroupId(strconv.Itoa(int(sys.Gid)))
		if err != nil {
			grp = &user.Group{Gid: strconv.Itoa(int(sys.Gid))}
		}
	}

	return Proc{PID: pid, User: *usr, Group: *grp}, nil
}

type Procs []Proc

func (p Procs) Len() int           { return len(p) }
func (p Procs) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p Procs) Less(i, j int) bool { return p[i].PID < p[j].PID }

type Proc struct {
	PID   int
	User  user.User
	Group user.Group
}

func (p Proc) Read(filename string) ([]byte, error) {
	const maxBufferSize = 1024 * 1024

	if !strings.HasPrefix(filename, filepath.Join(BasePath, strconv.Itoa(p.PID))) {
		filename = filepath.Join(BasePath, strconv.Itoa(p.PID), filename)
	}

	f, err := os.OpenFile(filename, os.O_RDONLY, 0444)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	reader := io.LimitReader(f, maxBufferSize)
	return io.ReadAll(reader)
}
