//go:build linux

/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package proc

import "strings"

func (p Proc) CmdLine() (string, error) {
	data, err := p.Read("cmdline")
	if err != nil {
		return "", err
	}

	return strings.TrimSpace(strings.ReplaceAll(string(data), "\x00", " ")), nil
}
